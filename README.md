# concurrency

- stages close their outbound channels when all the send operations are done
- stages keep receiving values from inbound channels until those channels are closed or the senders are unblocked
