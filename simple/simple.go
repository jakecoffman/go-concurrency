package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"sync"
	"time"
)

func Simple() {
	urls := []string{
		"https://golang.org",
		"https://www.ruby-lang.org",
		"https://www.python.org",
		"https://www.rust-lang.org",
		"https://dlang.org",
		"https://www.cplusplus.com",
		"https://ziglang.org",
		"https://www.javascript.com",
		"https://www.java.com",
	}

	wg := &sync.WaitGroup{}
	for _, url := range urls {
		wg.Add(1)
		go func(url string) {
			fetch(url)
			wg.Done()
		}(url)
	}

	wg.Wait()
}

func fetch(url string) {
	start := time.Now()

	res, err := http.Get(url)
	if err != nil {
		log.Fatalln(err)
	}
	ioutil.ReadAll(res.Body)
	res.Body.Close()

	log.Println(url, "TOOK", time.Now().Sub(start))
}

func main() {
	Simple()
}
