package main

import (
	"log"
	"math/rand"
	"sync"
	"time"
)

func fetch(url string) {
	log.Println("Starting", url)
	start := time.Now()

	time.Sleep(time.Second * time.Duration(rand.Intn(10)))
	//res, err := http.Get(url)
	//if err != nil {
	//	log.Fatalln(err)
	//}
	//ioutil.ReadAll(res.Body)
	//res.Body.Close()

	log.Printf("%v took %v", url, time.Now().Sub(start))
}

func main() {
	urls := []string{
		"https://golang.org",
		"https://www.ruby-lang.org",
		"https://www.python.org",
		"https://www.rust-lang.org",
		"https://dlang.org",
		"https://www.cplusplus.com",
		"https://ziglang.org",
		"https://www.javascript.com",
		"https://www.java.com",
	}

	wg := &sync.WaitGroup{}
	semaphore := make(chan struct{}, 5)
	defer close(semaphore)

	for _, url := range urls {
		wg.Add(1)
		go func(url string) {
			semaphore <- struct{}{}
			fetch(url)
			<- semaphore
			wg.Done()
		}(url)
	}

	wg.Wait()
}
