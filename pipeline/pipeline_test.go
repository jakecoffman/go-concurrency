package pipeline

import (
	"testing"
)

func TestPipeline(t *testing.T) {
	done := make(chan struct{})
	defer close(done)

	// Set up the pipeline.
	c := gen(2, 3)
	out := sq(done, c)

	// Consume the output.
	if v := <-out; v != 4 {
		t.Error(v)
	}
	if v := <-out; v != 9 {
		t.Error(v)
	}
}

func TestPipeline_Range(t *testing.T) {
	expected := []int{16, 81}
	done := make(chan struct{})
	defer close(done)

	// Set up the pipeline and consume the output.
	var i int
	for n := range sq(done, sq(done, gen(2, 3))) {
		if n != expected[i] {
			t.Error(n)
		}
		i++
	}
}

func TestFanInOut(t *testing.T) {
	done := make(chan struct{})
	defer close(done)

	in := gen(2, 3)

	// Distribute the sq work across two goroutines that both read from in. (Fan-Out)
	c1 := sq(done, in)
	c2 := sq(done, in)

	// Consume the merged output from c1 and c2 (Fan-In)
	for n := range merge(done, c1, c2) {
		if n != 4 && n != 9 {
			t.Error(n)
		}
	}
}
